# Use the official Ubuntu 22.04 as a base image
FROM ubuntu:22.04

# Set environment variables to avoid interactive prompts during package installation
ENV DEBIAN_FRONTEND=noninteractive

# Disable the problematic APT::Update::Post-Invoke script and install necessary packages
RUN apt-get update
RUN apt-get install -y wget curl jq

# Set the working directory
WORKDIR /app

# Set the entry point for the container
CMD ["bash"]
